import os
from slackclient import SlackClient 

slack_client = SlackClient(os.environ.get('GDAX_BOT_TOKEN'))

if __name__ == '__main__':
	api_call = slack_client.api_call("users.list")
	if api_call.get('ok'):
		# retrive all users so we can find our bot
		users = api_call.get('members')
		for user in users:
			print("Name: " + user["name"])
			print("ID: " + user["id"])