#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
import time
import gdax
import random
from slackclient import SlackClient

# Get bot and user IDs as an environment variables
BOT_ID = os.environ.get("GDAX_DEV_BOT_ID")
ADAM_ID = os.environ.get("ADAM_SLACK_ID")
SAM_ID = os.environ.get("SAM_SLACK_ID")
COREY_ID = os.environ.get("COREY_SLACK_ID")

# Correspond user IDs to user names
users = {SAM_ID: "sam", ADAM_ID: "adam", COREY_ID: "corey"}

# Sam's API info stored as environment variables
SAM_API_KEY = os.environ.get("GDAX_SAM_KEY")
SAM_API_SECRET = os.environ.get("GDAX_SAM_SECRET")
SAM_API_PW = os.environ.get("GDAX_SAM_PW")

# Adam's API info stored as environment variables
ADAM_API_KEY = os.environ.get("GDAX_ADAM_KEY")
ADAM_API_SECRET = os.environ.get("GDAX_ADAM_SECRET")
ADAM_API_PW = os.environ.get("GDAX_ADAM_PW")

# Corey's API info stored as environment variables
COREY_API_KEY = os.environ.get("GDAX_COREY_KEY")
COREY_API_SECRET = os.environ.get("GDAX_COREY_SECRET")
COREY_API_PW = os.environ.get("GDAX_COREY_PW")

# Constants
AT_BOT = "<@" + BOT_ID + ">"

# Instantiate Slack and Twilio clients
slack_client = SlackClient(os.environ.get('GDAX_DEV_BOT_TOKEN'))

# Instantiate user gdax client instances
user_instances = {}
user_instances["sam"] = gdax.AuthenticatedClient(SAM_API_KEY,SAM_API_SECRET, SAM_API_PW)
user_instances["adam"] = gdax.AuthenticatedClient(ADAM_API_KEY,ADAM_API_SECRET, ADAM_API_PW)
user_instances["corey"] = gdax.AuthenticatedClient(COREY_API_KEY,COREY_API_SECRET, COREY_API_PW)

gdax_public_client = gdax.PublicClient()

# Dictionary corresponding pairs to currency symbols
gdax_pairs = {"BTC-USD": "$", "BTC-EUR": "€", "BTC-GBP": "£", "ETH-USD": "$", 
              "ETH-BTC": "BTC ", "ETH-EUR": "€", "LTC-USD": "$", "LTC-BTC": "BTC ", 
              "LTC-EUR": "€"}

# Correspond key words to appropriate pairs
price_keys = {"BITCOIN": "BTC-USD", "BTC": "BTC-USD", "ETHER": "ETH-USD", 
              "ETH": "ETH-USD", "ETHEREUM": "ETH-USD", "LITECOIN": "LTC-USD", 
              "LTC": "LTC-USD"}

# Self explanatory
snark = ["Is English your first language?", "Try saying something intelligible.",
         "ASL classes not going well?", "Sorry, I could not parse your babbling.",
         "Yikes. That was incoherent. Try not drinking and Slacking.", "Does not compute.",
         "I care exactly zero about that."]

# Set default pair
DEFAULT_PAIR = "ETH-USD"


def start():
    """
        Start function allows the program to be run by external "long-running" python script
        for Python Anywhere purposes
    """
    # 1 second delay between reading from firehose
    READ_WEBSOCKET_DELAY = 1
    if slack_client.rtm_connect():
        print("GDAX Bot connected and running!")
        while True:
            command, channel, user_id = parse_slack_output(slack_client.rtm_read())
            if command and channel:
                handle_command(command, channel, user_id)
            time.sleep(READ_WEBSOCKET_DELAY)
    else:
        print("Connection failed. Invalid Slack token or bot ID?")

def parse_slack_output(slack_rtm_output):
    """
        The Slack Real Time Messaging API is an events firehose.
        this parsing function returns None unless a message is
        directed at the Bot, based on its ID.
    """
    output_list = slack_rtm_output
    if output_list and len(output_list) > 0:
        for output in output_list:
            if output and 'text' in output and AT_BOT in output['text']:
                # return text after the @ mention, whitespace removed
                return output['text'].split(AT_BOT)[1].strip().lower(), \
                       output['channel'], output['user']
    return None, None, None
    

def handle_command(command, channel, user_id):
    """
        This function parses and handles commands. First it gets all the words
        in the command then sets logic flags based on key words and combinations.
    """

    # Split command into words for parsing
    # Keep hyphenated words together so we can detect valid GDAX pairs
    words = re.findall(r"[\w-]+|[.,?!;:]", command)

    pair = DEFAULT_PAIR
    pairs_flag = 0 
    public_flag = 0

    DEFAULT_CMD = "default"
    ORDERS_CMD  = "orders"
    SELLS_CMD   = "sells"
    BUYS_CMD    = "buys"
    PONY_CMD    = "pony"
    LAMBO_CMD   = "lambo"

    command = DEFAULT_CMD

    # Parse commands and set logic flags
    for word in words:
        word = word.upper()
        if word in gdax_pairs:
            pairs_flag = 1
            pair = word
        elif word in price_keys:
            pairs_flag = 1
            pair = price_keys[word]
        elif word == "BUYS":
            command = BUYS_CMD
        elif word == "SELLS":
            command = SELLS_CMD
        elif word == "ORDERS":
            command = ORDERS_CMD
        elif word == "PUBLIC":
            public_flag = 1
        elif word == "PONY-UP":
            command = PONY_CMD
        elif word == "LAMBO":
            command = LAMBO_CMD

    # Execute based on logic flags set
    # If user mentioned pair and order type assume they want
    # order info for that pair
    if (command == ORDERS_CMD or command == BUYS_CMD or command == SELLS_CMD):
        orders(user_id, pair, channel, command, public_flag)

    elif (command == PONY_CMD):
        pony(channel)

    elif (command == LAMBO_CMD):
        lambo(channel)

    elif (pairs_flag):
        price(pair, channel)

    elif (command == DEFAULT_CMD):
        default(channel)

    # Debugging condition that can be removed later
    else:
        print("Slipped through the cracks")

# Get the price and post as standard message to channel
def price(pair, channel):
    price = gdax_public_client.get_product_ticker(pair)['price']
    response = gdax_pairs[pair] + price
    slack_client.api_call("chat.postMessage", channel=channel,
                          text=response, as_user=True)
 
def orders(user_id, pair, channel, command, public_flag):
    # Try statement checks for a valid user with API info in the users list
    # if it gets a key error it prints an error message and exist the order function
    try:
        # Use the user_id to get the user name and that to access the specific
        # user gdax instance to get current orders
        test = users[user_id]
        orders = user_instances[users[user_id]].get_orders()[0]
        pair = pair.upper()

        sells = []
        for order in orders:
            if order["side"] == "sell" and order["product_id"] == pair:
                sells.append((order["price"], order["size"]))
        sells = sorted(sells)
        
        buys = []
        for order in orders:
            if order["side"] == "buy" and order["product_id"] == pair:
                buys.append((order["price"], order["size"]))
        buys = sorted(buys, reverse=True)

        # If no buys or sells inform user and exit
        if (not len(buys) and not len(sells)):
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text="No open orders.")
            return 0

    except KeyError:
        slack_client.api_call("chat.postMessage", channel=channel,
                              text="No GDAX API info for this user.", as_user=True)
        return 0

    message = ""

    if(command == "orders"):
        message = "Buys: \n"
        for buy in buys:
            message = message + buy[0] + ", " + buy[1] + "\n"

        message = message + "Sells: \n"
        for sell in sells:
            message = message + sell[0] + ", " + sell[1] + "\n"

        if (not public_flag):
            slack_client.api_call("chat.postEphemeral", channel=channel, 
                                  text=message, user=user_id, as_user=True)
        else:
            slack_client.api_call("chat.postMessage", channel=channel, 
                                  text=message, as_user=True)
    
    elif (command == "buys"):
        # Check that there are buys
        if (not len(buys)):
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text="No open buys.")
            return 0            

        else:
            message = "Buys: \n"
            for buy in buys:
                message = message + buy[0] + ", " + buy[1] + "\n"
    
            if (not public_flag):
                slack_client.api_call("chat.postEphemeral", channel=channel, 
                                      text=message, user=user_id, as_user=True)
            else:
                slack_client.api_call("chat.postMessage", channel=channel, 
                                      text=message, as_user=True) 

    elif (command == "sells"):
        # Check that there are sells
        if (not len(sells)):
            slack_client.api_call("chat.postMessage", channel=channel,
                                  text="No open sells.")

        else:
            message = message + "Sells: \n"
            for sell in sells:
                message = message + sell[0] + ", " + sell[1] + "\n"

            if (not public_flag):
                slack_client.api_call("chat.postEphemeral", channel=channel, 
                                      text=message, user=user_id, as_user=True)
            else:
                slack_client.api_call("chat.postMessage", channel=channel, 
                                      text=message, as_user=True)     

# Print a pretty pony
def pony(channel):
    response = r'''
```
  ,  ,.~"""""~~..                                           ___
  )\,)\`-,       `~._                                     .'   ``._
  \  \ | )           `~._                   .-"""""-._   /         \
 _/ ('  ( _(\            `~~,__________..-"'          `-<           \
 )   )   `   )/)   )        \                            \           |
') /)`      \` \,-')/\      (                             \          |
(_(\ /7      |.   /'  )'  _(`                              |         |
    \\      (  `.     ')_/`                                |         /
     \       \   \                                         |        (
      \ )  /\/   /                                         |         `~._
       `-._)     |                                        /.            `~,
                 |                          |           .'  `~.          (`
                  \                       _,\          /       \        (``
                   `/      /       __..-i"   \         |        \      (``
                  .'     _/`-..--""      `.   `.        \        ) _.~<``
                .'    _.j     /            `-.  `.       \      '=< `
              .'   _.'   \    |               `.  `.      \
             |   .'       ;   ;               .'  .'`.     \
             \_  `.       |   \             .'  .'   /    .'
               `.  `-, __ \   /           .'  .'     |   (
                 `.  `'` \|  |           /  .-`     /   .'
                   `-._.--t  ;          |_.-)      /  .'
                          ; /           \  /      / .'
                         / /             `'     .' /
                        /,_\                  .',_(
                       /___(                 /___( 
```
    '''
    slack_client.api_call("chat.postMessage", channel=channel,
                          text=response, as_user=True)

# Print a lambo
def lambo(channel):
    response = r'''
```
             __---~~~~--__                      __--~~~~---__
            `\---~~~~~~~~\\                    //~~~~~~~~---/'
              \/~~~~~~~~~\||                  ||/~~~~~~~~~\/
                          `\\                //'
                            `\\            //'
                              ||          ||
                    ______--~~~~~~~~~~~~~~~~~~--______
               ___ // _-~                        ~-_ \\ ___
              `\__)\/~                              ~\/(__/'
               _--`-___                            ___-'--_
             /~     `\ ~~~~~~~~------------~~~~~~~~ /'     ~\
            /|        `\         ________         /'        |\
           | `\   ______`\_      \------/      _/'______   /' |
           |   `\_~-_____\ ~-________________-~ /_____-~_/'   |
           `.     ~-__________________________________-~     .'
            `.      [_______/------|~~|------\_______]      .'
             `\--___((____)(________\/________)(____))___--/'
              |>>>>>>||                            ||<<<<<<|
              `\<<<<</'                            `\>>>>>/' 
```
    '''
    slack_client.api_call("chat.postMessage", channel=channel,
                          text=response, as_user=True)

def default(channel):
    # Set random snarky response
    response = snark[random.randrange(len(snark))]
    slack_client.api_call("chat.postMessage", channel=channel,
                          text=response, as_user=True)

if __name__ == '__main__':
    start()
