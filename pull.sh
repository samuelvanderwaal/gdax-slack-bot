#!/bin/bash

SLACK_BOT_PY="$HOME/.virtualenvs/gdax-slack-bot/bin/python3.6 ./long-running.py"
LOCKFILE="/tmp/pull.lock"

start_py() {
  $SLACK_BOT_PY &
  PY_PID="$!"
  echo "python pid: $PY_PID"
}

kill_py() {
  kill $PY_PID
}

restart_py() {
  kill_py
  start_py
}

setup() {
  local REPO_DIR="$HOME/gdax-slack-bot/"
  if [[ -d $REPO_DIR ]]; then
    cd $HOME/gdax-slack-bot/
  fi

  if [[ -f $LOCKFILE ]]; then
    echo "Already running"
    exit 1
  fi

  touch $LOCKFILE

  trap "{ rm -f $LOCKFILE ; kill_py ; exit 255; }" EXIT
  set -e

  # Start slack bot
  start_py
}

update_restart() {
  touch $LOCKFILE
  branch=$(git branch | grep "*" | awk '{ print $2 }')
  git checkout master &>/dev/null
  commit=$(git rev-parse HEAD)
  bot_md5=$(md5sum ./gdaxbot.py)
  long_md5=$(md5sum ./long-running.py)
  pull_md5=$(md5sum ./pull.sh)
  git pull origin &>/dev/null
  if [[ "$commit" != "$(git rev-parse HEAD)" ]]; then
    echo "New commits on master."
    if [[ "$pull_md5" != $(md5sum ./pull.sh) ]]; then
      echo "./pull.sh updated. Restarting..."
      rm -f $LOCKFILE
      kill_py
      bash ./pull.sh &
      disown
      exit 0
    fi
    if [[ "$bot_md5" != $(md5sum ./gdaxbot.py) ||
          "$long_md5" != $(md5sum ./long-running.py) ]]; then
      echo "./gdaxbot.py updated. Restarting..."
      restart_py
    fi
  fi
  git checkout $branch &>/dev/null
}

setup
while [ 1 ]; do
  update_restart
  sleep 30
done
